vim.wo.number = true

vim.opt.showcmd = true
vim.opt.cursorline = true

vim.opt.tabstop = 2
